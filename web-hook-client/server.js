const express = require('express');
const http = require('http');
const cors = require('cors');
const bodyParser = require('body-parser');
const router = express.Router();
const appRoutes = require('./routes/api')(router);

const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use('', appRoutes);
const server = http.createServer(app);

server.listen(3002, function () {
    console.log(`listening on port ${3002}`);
});
