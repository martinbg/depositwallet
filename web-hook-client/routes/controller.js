function webHook(req, res) {
  const body = req.body;
  console.log(body);
  res.send();
}

module.exports = (() => {
  return {
    webHook
  }
})()