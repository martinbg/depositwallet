const controller = require('./controller');

module.exports = function (router) {
    router.post('/webHook', controller.webHook);

    return router;
}