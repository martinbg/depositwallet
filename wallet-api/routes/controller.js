const { addWallet } = require('../data/data-service');
const { moveAllEther, moveEther, moveAllErc20, moveErc20 } = require('../move-funds/move-funds.service');
const web3 = require('web3');

//Initializing web3
const web3Provider = new web3('https://ropsten.infura.io/v3/9aa3d95b3bc440fa88ea12eaa4456161')

// Creating wallet and adds it to our data structures
function createWallet(req, res) {
  const userId = req.body.userId;
  const wallet = web3Provider.eth.accounts.create();
  addWallet({
    privateKey: wallet.privateKey,
    walletAddress: wallet.address,
    userId
  });
  res.send({ walletAddress: wallet.address });
}

// I am well aware it is not safe to expose the following routes
// but my main focus is on the blockchain functionality

async function moveEth(req, res) {
  const { userId, to, amount } = req.body;
  const receipt = await moveEther(amount, userId, to);
  res.send(receipt);
}

async function moveAllEth(req, res) {
  const { userId, to } = req.body;
  const receipt = await moveAllEther(userId, to);
  res.send(receipt);
}

async function moveErc20Tokens(req, res) {
  const { userId, to, amount, contractAddress } = req.body;
  const receipt = await moveErc20(amount, contractAddress, userId, to);
  res.send(receipt);
}

async function moveAllErc20Tokens(req, res) {
  const { userId, to, contractAddress } = req.body;
  const receipt = await moveAllErc20(contractAddress, userId, to);
  res.send(receipt);
}

module.exports = (() => {
  return {
    createWallet,
    moveEth,
    moveAllEth,
    moveErc20Tokens,
    moveAllErc20Tokens
  }
})()