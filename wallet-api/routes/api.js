const controller = require('./controller');

module.exports = function (router) {
    router.post('/createWallet', controller.createWallet);
    router.post('/moveEth', controller.moveEth);
    router.post('/moveAllEth', controller.moveAllEth);
    router.post('/moveErc20Tokens', controller.moveErc20Tokens);
    router.post('/moveAllErc20Tokens', controller.moveAllErc20Tokens);

    return router;
}