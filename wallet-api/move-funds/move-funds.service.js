// This service will have the functionality for sending funds from our client wallet to another

var Web3 = require('web3');

// the rpc url should be set in a configuration file not to be set in code statically
const web3 = new Web3('https://ropsten.infura.io/v3/9aa3d95b3bc440fa88ea12eaa4456161');
const { getUserData } = require('../data/data-service');

async function moveAllEther(userId, to) {
    const { walletAddress, privateKey } = getUserData(userId);
    const etherBalance = await getEtherBalance(walletAddress);
    const etherBalanceBN = web3.utils.toBN(etherBalance);
    const gasLimit = await web3.eth.estimateGas({ from: walletAddress, to: to });
    const gasPrice = await web3.eth.getGasPrice();
    const gasFeeBN = caluclateTxGasFee(gasLimit, gasPrice);
    const finalAmount = etherBalanceBN.sub(gasFeeBN);
    return await _moveEther(finalAmount, walletAddress, privateKey, to, gasPrice, gasLimit);
}


async function moveEther(amount, userId, to) {
    const { walletAddress, privateKey } = getUserData(userId);
    const etherBalance = await getEtherBalance(walletAddress);
    const etherBalanceBN = web3.utils.toBN(etherBalance);
    const gasLimit = await web3.eth.estimateGas({ from: walletAddress, to: to });
    const gasPrice = await web3.eth.getGasPrice();
    const gasFeeBN = caluclateTxGasFee(gasLimit, gasPrice);
    const weiAmountBN = web3.utils.toBN(web3.utils.toWei(amount, "ether"));
    // this check is not needed because it is implemented
    // in sendSignedTransaction in _moveEther
    if (weiAmountBN.add(gasFeeBN).gt(etherBalanceBN)) {
        throw "Insufficient funds!";
    }
    return await _moveEther(weiAmountBN, walletAddress, privateKey, to, gasPrice, gasLimit);
}

async function getEtherBalance(walletAddress) {
    const balance = await web3.eth.getBalance(walletAddress);
    return balance;
}

function caluclateTxGasFee(gasLimit, gasPrice) {
    const limitBN = web3.utils.toBN(gasLimit);
    const priceBN = web3.utils.toBN(gasPrice);
    const total = limitBN.mul(priceBN);
    return total;
}

//internal function for sending ether
async function _moveEther(amount, from, privateKey, to, gasPrice, gasLimit) {
    try {
        const rawTransaction = {
            nonce: await web3.eth.getTransactionCount(from),
            gas: gasLimit,
            gasPrice: gasPrice,
            chainId: 3,
            from,
            to,
            value: amount
        }

        const signedTx = await web3.eth.accounts.signTransaction(rawTransaction, privateKey);
        const res = await web3.eth.sendSignedTransaction(signedTx.rawTransaction);
        return res;
    }
    catch (err) {
        console.log(err);
    }
}

async function moveAllErc20(contractAddress, userId, to) {
    const { walletAddress, privateKey } = getUserData(userId);
    const balance = await getErc20Balance(walletAddress, contractAddress);
    return await _moveErc20(balance, walletAddress, privateKey, contractAddress, to);
}

// I assume amount to be in the correct format
// There could be decimal transformation if decimal is provided by the contract
async function moveErc20(amount, contractAddress, userId, to) {
    const { walletAddress, privateKey } = getUserData(userId);
    const balance = await getErc20Balance(walletAddress, contractAddress);
    const balanceBN = web3.utils.toBN(balance);
    const amountBN = web3.utils.toBN(amount);
    if (amountBN.gt(balanceBN)) {
        throw "Not enough tokens in wallet";
    }
    return await _moveErc20(amount, walletAddress, privateKey, contractAddress, to);
}

async function getErc20Balance(walletAddress, contractAddress) {
    const abi = [
        {
            constant: true,
            inputs: [{ name: "_owner", type: "address" }],
            name: "balanceOf",
            outputs: [{ name: "balance", type: "uint256" }],
            type: "function",
        },

    ];

    const contract = new web3.eth.Contract(abi, contractAddress);
    const balance = await contract.methods.balanceOf(walletAddress).call();
    return balance;
}

//internal function for sending erc20 tokens
async function _moveErc20(amount, from, privateKey, contractAddress, to) {
    const data = generateErc20TxData(amount, to);

    try {
        const nonce = await web3.eth.getTransactionCount(from);
        // I tried to calculate gas price but it always said it is not enough
        // So i set a static value of 200000. This is not a problem because 
        // if the fees are lower we will receive the difference back
        const rawTransaction = {
            nonce,
            chainId: 3,
            from,
            to: contractAddress,
            data,
            gas: 200000
        }

        const signedTx = await web3.eth.accounts.signTransaction(rawTransaction, privateKey);
        const res = await web3.eth.sendSignedTransaction(signedTx.rawTransaction);
        return res;
    }
    catch (err) {
        console.log(err);
    }
}

function generateErc20TxData(amount, to) {
    const functionId = '0xa9059cbb';
    //to is 20 bytes so 40 hex characters without '0x'
    //we need to add 24 leading hex zeros so the input is 32 bytes
    const recipient = '0'.repeat(24) + to.slice(2);
    const hexAmount = parseInt(amount, 10).toString(16);
    //we need to add the needed number of leading zeros so the input is 32 bytes
    const finalAmount = '0'.repeat(64 - hexAmount.length) + hexAmount;
    const final = functionId + recipient + finalAmount;
    return final;
}

module.exports = { moveAllEther, moveEther, moveAllErc20, moveErc20 };