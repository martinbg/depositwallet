//This service is used for maintaining two dictionaries

// The data could be loaded from a db on startup
// The data could be saved to a db 

// We will need the walletDataMapping to check if a 
// specific wallet address is from our api and what userId it is corresponding to
const walletDataMapping = {};

// We will need the userDataMapping to get easily
// the private key and address of a specific user so that we can move his deposits
const userDataMapping = {};

function addWallet({ userId, walletAddress, privateKey }) {
    userDataMapping[userId] = {
        walletAddress,
        privateKey
    };
    walletDataMapping[walletAddress] = userId;
}

function isApiWalletAddress(walletAddress) {
    return walletAddress.toLowerCase() in walletDataMapping;
}

function getUserId(walletAddress) {
    return walletDataMapping[walletAddress];
}

function getUserData(userId) {
    return userDataMapping[userId];
}

module.exports = ({ addWallet, isApiWalletAddress, getUserId, getUserData });