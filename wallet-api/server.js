const express = require('express');
const http = require('http');
const cors = require('cors');
const bodyParser = require('body-parser');
const router = express.Router();
const appRoutes = require('./routes/api')(router);
const { startMonitoring } = require('./monitoring/monitoring-service');

const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use('/api', appRoutes);
const server = http.createServer(app);

server.listen(3001, function () {
    console.log(`listening on port ${3001}`);
});

// Could be started with a configuration
// Could be in a different application
startMonitoring();