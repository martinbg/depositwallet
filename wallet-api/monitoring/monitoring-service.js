
// This service is responsible for monitoring the blockchain and sending information to specified webhook

var axios = require('axios');
var Web3 = require('web3');

// the rpc url should be set in a configuration file not to be set in code statically
const web3 = new Web3('https://ropsten.infura.io/v3/9aa3d95b3bc440fa88ea12eaa4456161');
const { isApiWalletAddress, getUserId } = require('../data/data-service');

let lastBlocNumber = -1;
let blockTime = 5000;

async function processBlock(blockNumber) {
    let block = await web3.eth.getBlock(blockNumber);
    for (const transactionHash of block.transactions) {
        // Checking if the transaction is valid
        // If there is no receipt or the status is not true then the transaction is pending or failed
        let transactionReceipt = await web3.eth.getTransactionReceipt(transactionHash);
        if (transactionReceipt == null || !transactionReceipt.status) {
            continue;
        }

        let transaction = await web3.eth.getTransaction(transactionHash);
        // Checking if the to address is from our api's wallet addresses
        // If so it is an etherium deposit to that wallet
        if (isApiWalletAddress(transaction.to)) {
            send(transactionHash, transaction.value, 'Ether', null, blockNumber, getUserId(transaction.to));
            continue;
        }

        // If it is not an etherium deposit we procceed to check if it is an erc20 deposit
        // If the input is empty so it is not a call to a contract
        if (transaction.input == '0x') {
            continue;
        }

        // '0xa9059cbb' is the function id for the standart erc20 transfer function
        // If it matches it may be a transfer deposit to our wallet
        if (transaction.input.startsWith('0xa9059cbb')) {
            //Check for valid input
            if (transaction.input.length < 74) {
                continue;
            }
            // Decoding arguments to see the recipient address
            const recepient = '0x' + transaction.input.slice(34, 74);
            const hexAmount = '0x' + transaction.input.slice(74);
            const amount = parseInt(hexAmount, 16);
            if (isApiWalletAddress(recepient)) {
                send(transactionHash, amount, 'ERC20', transaction.to, blockNumber, getUserId(recepient));
                continue;
            }
        }
    }
    lastBlocNumber = blockNumber;
}

// sending data to the webHook
// this can be implemented to support multiple webHooks
// and it should not be statically set in the code
// it should be in configuration or added dynamically in real time on request
async function send(blockchainTxId, amount, type, tokenAddress, blockNumber, userId) {
    axios
        .post('http://localhost:3002/webHook', {
            blockchainTxId,
            amount,
            type,
            tokenAddress,
            blockNumber,
            userId
        })
        .catch(error => {
            console.error(error)
        });
}

// Processing blockchain blocks one by one and wait if there is no new one
async function checkCurrentBlock() {
    const currentBlockNumber = await web3.eth.getBlockNumber()
    while (lastBlocNumber == -1 || currentBlockNumber > lastBlocNumber) {
        await processBlock(lastBlocNumber == -1 ? currentBlockNumber : lastBlocNumber + 1);
    }
    setTimeout(checkCurrentBlock, blockTime);
}

function startMonitoring() {
    checkCurrentBlock();
}

module.exports = ({ startMonitoring });