# Deposit wallet #

The main focus to this project is to create wallets, the ability to monitor when deposit transactions (Ether and ERC20) are made to them and provide the ability to move the funds from them to another wallet. 

## Wallet-Api
The wallet-api contains the following functionality provided with api endpoints:

*  createWallet -> Creating a wallet and returning the wallet address
*  moveEth -> Sending an amount of Ether to a specific wallet address by providing the user id
*  moveEth -> Sending all available Ether to a specific wallet address by providoing user id
*  moveErc20Tokens -> Sending an amount of Tokens corresponding to a specific contract to a specific address by providing user id
*  moveAllErc20Tokens -> Sending all available Tokens correspondening to a specific contract to a specific address by providing user id

## Montoring service
The wallet api contains also the monitoring service, which starts on startup.
It watches the blockchain and reads every transaction and filters them by the created wallets.

## Web-hook-client
It is only used for testing the monitoring functionality by providing it as a web hook


## Technology used
* NodeJS
* web3